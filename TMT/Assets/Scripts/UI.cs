﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public Text lifes;
    public Text rupies;
    public Text heal;
    public Text keys;
    public Text bombs;
    public Text ammo;
    private int intBombs;
    private int intAmmo;
    private int intKeys;
    private int intHeal;
    private int intLifes;
    private int intRupies;
    
    // Start is called before the first frame update
    void Start()
    {
        intLifes = GameManager._instance.getLifes();
        lifes.text = intLifes.ToString();
        intHeal = GameManager._instance.getHeal();
        heal.text = intHeal.ToString();
        intRupies = GameManager._instance.getRupies();
        heal.text = intHeal.ToString();
        intKeys = GameManager._instance.getKeys();
        keys.text= intKeys.ToString();
        intBombs = GameManager._instance.getBombs();
        bombs.text = intBombs.ToString();
        intAmmo = GameManager._instance.getAmmo();
        ammo.text = intAmmo.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager._instance.getLifes()!=intLifes){
            intLifes = GameManager._instance.getLifes();
            lifes.text = intLifes.ToString();
        }

        if(GameManager._instance.getHeal()!=intHeal){
            intHeal = GameManager._instance.getHeal();
            heal.text = intHeal.ToString();
        }

        if(GameManager._instance.getRupies()!=intRupies){
            intRupies= GameManager._instance.getRupies();
            rupies.text= intRupies.ToString();
        }

        if(GameManager._instance.getKeys()!=intKeys){
            intKeys = GameManager._instance.getKeys();
            keys.text= intKeys.ToString();
        }

        if(GameManager._instance.getBombs()!=intBombs){
            intBombs = GameManager._instance.getBombs();
            bombs.text = intBombs.ToString();
        }        

        if(GameManager._instance.getAmmo()!=intAmmo){
            intAmmo = GameManager._instance.getAmmo();
            ammo.text = intAmmo.ToString();
        }
    }
}
