﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseInteractions : MonoBehaviour
{
    private Animator _animator;
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void Update()
    {

    }

    private void OnTriggerStay(Collider collision)
    {
        //Open Chests
        if (collision.transform.tag == "chest" && Input.GetButtonDown("CloseInteractions"))
        {
            ChestContent chestContent = collision.gameObject.GetComponent<ChestContent>();

            if (chestContent.greenRupy)
            {
                GameManager._instance.rupies += 1;
                chestContent.greenRupy = false;
            }

            else if (chestContent.redRupy)
            {
                GameManager._instance.rupies += 5;
                chestContent.redRupy = false;
            }

            else if (chestContent.blueRupy)
            {
                GameManager._instance.rupies += 10;
                chestContent.blueRupy = false;
            }

            else if (chestContent.key)
            {
                GameManager._instance.keys += 1;
                chestContent.key = false;
            }

            else if (chestContent.medicalHerb)
            {
                GameManager._instance.medialHerbs += 2;
                chestContent.medicalHerb = false;
            }
        }

        //Open key doors
        else if (collision.transform.tag == "keyDoor" && Input.GetButtonDown("CloseInteractions"))
        {
            if (GameManager._instance.keys > 0)
            {
                collision.transform.position += new Vector3(0, 5, 0);
                --GameManager._instance.keys;
            }
        }

        //MeleAtack
        else if (collision.transform.tag == "enemy" && Input.GetButtonDown("CloseInteractions"))
        {
            _animator.SetTrigger("knife");
            HpEnemy hpEnemy = collision.gameObject.GetComponent<HpEnemy>();
            hpEnemy.hp -= GameManager._instance.atackLv;
            collision.attachedRigidbody.AddRelativeForce(collision.transform.position, ForceMode.Impulse);
            if (hpEnemy.hp <= 0)
            {
                Destroy(collision.gameObject);
                Destroy(hpEnemy);
            }
        }

        else if (collision.transform.tag == "enemy" && !(Input.GetButtonDown("CloseInteractions")))
        {
           _animator.SetTrigger("damage");
        }

        //PurchaseObjects
        else if ((collision.transform.tag == "ammo" || collision.transform.tag == "medicalHerb" || collision.transform.tag == "bomb"
        || collision.transform.tag == "atackLvUp" || collision.transform.tag == "defenseLvUp") && Input.GetButtonDown("CloseInteractions"))
        {
            if (collision.transform.tag == "ammo" && GameManager._instance.rupies - GameManager._instance.ammoPrice >= 0)
            {
                GameManager._instance.ammo += 15;
                GameManager._instance.rupies -= GameManager._instance.ammoPrice;
            }

            else if (collision.transform.tag == "medicalHerb" && GameManager._instance.rupies - GameManager._instance.medicalHerbPrice >= 0)
            {
                GameManager._instance.medialHerbs += 5;
                GameManager._instance.rupies -= GameManager._instance.medicalHerbPrice;
            }

            else if (collision.transform.tag == "bomb" && GameManager._instance.rupies - GameManager._instance.bombPrice >= 0)
            {
                GameManager._instance.bomb += 5;
                GameManager._instance.rupies -= GameManager._instance.bombPrice;
            }

            else if (collision.transform.tag == "atackLvUp" && GameManager._instance.rupies - GameManager._instance.atackLevelUpPrice >= 0)
            {
                GameManager._instance.atackLv += 1;
                GameManager._instance.rupies -= GameManager._instance.atackLevelUpPrice;
                Destroy(collision.gameObject);
            }

            else if (collision.transform.tag == "defenseLvUp" && GameManager._instance.rupies - GameManager._instance.defenseLevelUpPrice >= 0)
            {
                GameManager._instance.defenseLv += 1;
                GameManager._instance.rupies -= GameManager._instance.defenseLevelUpPrice;
                Destroy(collision.gameObject);
            }
        }
    }
}
