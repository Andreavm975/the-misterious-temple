﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Heal();
    }

    void Heal()
    {
        if (Input.GetButtonDown("Heal"))
        {
            if (GameManager._instance.medialHerbs > 0)
            {
                if (GameManager._instance.hp != 10)
                {
                    if (GameManager._instance.hp + 3 < 10)
                    {
                        GameManager._instance.hp += 3;
                        --GameManager._instance.medialHerbs;
                    }
                    else
                    {
                        GameManager._instance.hp += (10 - GameManager._instance.hp);
                        --GameManager._instance.medialHerbs;
                    }
                }
            }
        }
    }
}
