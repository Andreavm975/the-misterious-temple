﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCharacterMovement : MonoBehaviour
{
    public float speed = 0f;
    private Vector3 displacement;
    private Rigidbody rigidbody;
    public int jumpHeigh = 0;
    public bool isOnGround = true;
    private Animator _animator;
    public GameObject winPanel;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        _animator = GetComponent<Animator>();
        winPanel.SetActive(false);
    }

    void Update()
    {
        Walk();
        Jump();
        ShootAnim();
        DeathAnim();
    }

    void ShootAnim()
    {
        if (Input.GetButtonDown("Fire") && GameManager._instance.ammo != 0)
        {
            _animator.SetTrigger("shoot");
        }
    }

    void DeathAnim()
    {
        if (GameManager._instance.hp <= 0)
        {
            _animator.SetBool("death", true);
        }
    }

    void Walk()
    {
        displacement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        transform.position += (displacement * (speed * Time.deltaTime));

        if (Input.GetAxis("Horizontal") < 0)
        {
            transform.rotation = Quaternion.Euler(0, -90, 0);
            _animator.SetBool("moving", true);
        }

        else if (Input.GetAxis("Horizontal") > 0)
        {
            transform.rotation = Quaternion.Euler(0, 90, 0);
            _animator.SetBool("moving", true);
        }

        else if (Input.GetAxis("Vertical") < 0)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
            _animator.SetBool("moving", true);
        }

        else if (Input.GetAxis("Vertical") > 0)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            _animator.SetBool("moving", true);
        }

        else _animator.SetBool("moving", false);
    }

    void Jump()
    {
        if (Input.GetButtonDown("Jump") && isOnGround)
        {
            rigidbody.AddForce(new Vector3(0, jumpHeigh, 0), ForceMode.Impulse);
            isOnGround = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 9)
        {
            isOnGround = true;
        }
    }

    private void OnTriggerStay(Collider collision)
    {
        if (collision.transform.tag == "win" && Input.GetButtonDown("CloseInteractions"))
        {
            Debug.Log("Win");
            winPanel.SetActive(true);
            GameManager._instance.setGameState(GameStates.win);
        }
    }
}
