﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBomb : MonoBehaviour
{
    public GameObject bomb;

    void Start()
    {
        
    }

    void Update()
    {
        PlaceBomb();
    }

    void PlaceBomb()
    {
        if(Input.GetButtonDown("SetBomb"))
        {
            GameObject instBomb = Instantiate(bomb, transform.position + new Vector3(0,-0.4f,0), Quaternion.identity) as GameObject;
            Destroy(instBomb, 2);
        }
    }
}
