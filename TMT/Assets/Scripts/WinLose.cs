﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinLose : MonoBehaviour
{
    public GameObject winPanel;
    public GameObject losePanel;
    

    // Start is called before the first frame update
    void Start()
    {
       losePanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager._instance.hp <= 0){
            losePanel.SetActive(true);
            GameManager._instance.setGameState(GameStates.lose);
            Debug.Log("Ahh me muero!!");
        }


        
    }

    public void onTryAgainClicked(){
        if (SaveLoadManager.SaveExists()) {
            SaveLoadManager.DeleteSave();
        }
        rechargeVariables(); 
        GameManager._instance.setGameState(GameStates.playing);
        SceneManager.LoadScene("Scene1");
       
    }

    public void onBackToMenuClickedLose(){
        if (SaveLoadManager.SaveExists()) {
            SaveLoadManager.DeleteSave();
        }
        rechargeVariables();
        GameManager._instance.setGameState(GameStates.mainMenu);
        SceneManager.LoadScene("MainMenu");
        
    }

    public void rechargeVariables(){
        GameManager._instance.hp = 10;
        GameManager._instance.rupies = 0;
        GameManager._instance.keys = 0;
        GameManager._instance.medialHerbs = 0;
        GameManager._instance.ammo = 15;
        GameManager._instance.bomb = 0;
        GameManager._instance.atackLv = 1;
        GameManager._instance.defenseLv = 1;
    }

}
