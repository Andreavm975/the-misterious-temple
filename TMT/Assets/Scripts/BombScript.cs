﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombScript : MonoBehaviour
{
    public GameObject bomb;
    public int power = 3;
    public float radius = 7f;
    //public float upForce = 1f;
    GameObject bombInst;
    void Start()
    {
        
    }

    private void Update()
    {
        if (Input.GetButtonDown("SetBomb"))
        {
            StartCoroutine(SetBomb());
        }
        //Debug.DrawRay(bombInst.transform.position, Vector3.forward*radius);
    }
    IEnumerator SetBomb()
    {
        if (GameManager._instance.bomb > 0)
        {
            --GameManager._instance.bomb;
            bombInst = Instantiate(bomb, transform.position, Quaternion.identity);
            
            yield return new WaitForSeconds(2);
            Detonate();
            Destroy(bombInst);
        }
    }

    void Detonate()
    {
        Vector3 explosionPosition = bombInst.transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPosition, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null)
            {
                if (rb.gameObject.transform.tag == "Player")
                {
                    GameManager._instance.hp -= power;
                }
                else if (rb.gameObject.transform.tag == "enemy")
                {
                    HpEnemy hpEnemy = rb.gameObject.GetComponent<HpEnemy>();
                    hpEnemy.hp -= power;
                }
                //hit.gameObject.SetActive(false);
                //rb.AddExplosionForce(power, explosionPosition, radius, upForce, ForceMode.Impulse);
                //Destroy(hit.gameObject);
            }
        }
    }
}
