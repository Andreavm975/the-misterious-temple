﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpEnemy : MonoBehaviour
{
    public int hp = 0;
    public int atackPower = 0;

    public static HpEnemy enemigo;

    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        if (hp <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player" && !Input.GetButtonDown("CloseInteractions"))
        {
            GameManager._instance.hp -= (atackPower - GameManager._instance.defenseLv);
            collision.rigidbody.AddRelativeForce(collision.transform.position, ForceMode.Impulse);
        }
    }
}
