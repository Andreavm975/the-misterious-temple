﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausePanel : MonoBehaviour
{
    //[SerializeField]
    public GameObject pausePanel;
    public GameObject UIpanel;

    private bool isPaused;
  

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            isPaused = !isPaused;
        }

        if (isPaused)
        {
            ActivateMenu();
        } else
        {
            DeactivateMenu();
        }
    }


    public void ActivateMenu()
    {
        GameManager._instance.setGameState(GameStates.pause);
        pausePanel.SetActive(true);
        UIpanel.SetActive(false);

    }

    public void DeactivateMenu()
    {
        GameManager._instance.setGameState(GameStates.playing);
        UIpanel.SetActive(true);
        pausePanel.SetActive(false);
        
    }
    public void OnResumeClicked()
    {
        isPaused = false;
        GameManager._instance.setGameState(GameStates.playing);
        
    }

    public void OnRestartClicked()
    {
        rechargeVariables();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        GameManager._instance.setGameState(GameStates.playing);
        
    }


    public void OnBackToMenuClicked()
    {
        rechargeVariables();
        SceneManager.LoadScene(0);
        GameManager._instance.setGameState(GameStates.mainMenu);
    }

        public void rechargeVariables(){
        GameManager._instance.hp = 10;
        GameManager._instance.rupies = 0;
        GameManager._instance.keys = 0;
        GameManager._instance.medialHerbs = 0;
        GameManager._instance.ammo = 15;
        GameManager._instance.bomb = 0;
        GameManager._instance.atackLv = 1;
        GameManager._instance.defenseLv = 1;
    }
}
