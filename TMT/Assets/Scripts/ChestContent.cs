﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestContent : MonoBehaviour
{
    public static ChestContent chestContent;

    public bool greenRupy;
    public bool redRupy;
    public bool blueRupy;
    public bool key;
    public bool medicalHerb;

    private Animator _animator;
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Player" && Input.GetButtonDown("CloseInteractions"))
        {
            _animator.SetBool("open", true);
        }
    }
}
