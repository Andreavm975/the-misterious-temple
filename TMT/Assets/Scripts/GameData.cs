﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData
{
    public int[] gameData;

    public GameData(int hp, int rupies,int medialHerbs, int ammo,int bomb, int keys, int atackLv, int defenseLv)
    {
        gameData = new int[8];
        gameData[0] = hp;
        gameData[1] = rupies;
        gameData[2] = medialHerbs;
        gameData[3] = ammo;
        gameData[4] = bomb;
        gameData[5] = keys;
        gameData[6] = atackLv;
        gameData[7] = defenseLv;
    }
}
