﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MummyScript : MonoBehaviour
{
    private float _distanceFromCharacter = 15.0f;

    private Animator _animator;

    private float _timeLeft = 0.0f;

    GameObject player;
    NavMeshAgent enemy;

    // Start is called before the first frame update
    void Start()
    {

        _animator = GetComponent<Animator>();

        player = GameObject.FindGameObjectWithTag("Player");

        if (player == null)
        {

        }

        enemy = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        enemy.destination = new Vector3(player.transform.position.x + 1.5f, player.transform.position.y, player.transform.position.z);

        if (enemy.remainingDistance < _distanceFromCharacter)
        {
            _animator.SetBool("WalkFWD", true);
            enemy.Resume();
            if (enemy.remainingDistance < 4)
            {
                _animator.SetBool("crippled", true);
            }
            else
            {
                _animator.SetBool("crippled", false);
            }

        }
        else
        {
            enemy.Stop();
            _animator.SetBool("WalkFWD", false);
        }

        //if (health <= 0f)
        //{
        //    _animator.SetBool("Attack", false);
        //    _animator.SetBool("Walk", false);
        //    _animator.SetBool("Idle", false);
        //    enemy.Stop();
        //}
    }
}
