﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombParticles : MonoBehaviour
{
    private ParticleSystem ps;
    public float hSliderValue = 0.0F;
    void Start()
    {
        ps = GetComponent<ParticleSystem>();

    }

    // Update is called once per frame
    void Update()
    {
        var main = ps.main;
        main.startDelay = hSliderValue;
        //ps.Play();
    }
}
