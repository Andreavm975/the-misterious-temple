﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour
{
    public GameObject door;
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.transform.tag == "Player" && Input.GetButtonDown("CloseInteractions"))
        {
            door.transform.position += new Vector3(0, 5, 0);
        }
    }
}
