﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject bullet;
    private Animator _animator;

    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    void Shoot()
    {
        if (Input.GetButtonDown("Fire") && GameManager._instance.ammo != 0)
        {
            --GameManager._instance.ammo;
            GameObject instBullet = Instantiate(bullet, transform.position, Quaternion.identity) as GameObject;
            Rigidbody instBulletRigidBody = instBullet.GetComponent<Rigidbody>();
            instBulletRigidBody.velocity = transform.forward * (-30f);
            Destroy(instBullet, 0.5f);
        }
    }

    void Update()
    {
      Shoot();
    }
}
