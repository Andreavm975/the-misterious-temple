﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameStates
{
    playing,
    pause,
    win,
    lose,
    mainMenu
}

public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public GameStates gameState;
    public GameObject winPanel;

    //Variables
    public int hp = 10;
    public int rupies = 0;
    public int keys = 0;
    public int medialHerbs = 0;
    public int ammo = 15;
    public int bomb = 0;
    public int atackLv = 1;
    public int defenseLv = 1;

    public int medicalHerbPrice = 5;
    public int bombPrice = 4;
    public int ammoPrice = 3;
    public int atackLevelUpPrice = 10;
    public int defenseLevelUpPrice = 10;

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

        gameState = GameStates.mainMenu;
    }

    void Start()
    {
    }

    public int getBombs(){
        return bomb;
    }

    public int getAmmo(){
        return ammo;
    }

    public int getKeys(){
        return keys;
    }

    public int getHeal (){
        return medialHerbs;
    }

    public int getLifes(){
        return hp;
    }

    public int getRupies(){
        return rupies;
    }


    void Update()
    {
        gameStateUpdate();
        
    }

    void gameStateUpdate()
    {
        switch (gameState)
        {
            case GameStates.pause:
                setGameInPause();
                break;
            case GameStates.win:
                setGameInWin();
                break;
            case GameStates.mainMenu:
                setGameInMainMenu();
                break;
            case GameStates.lose:
                setGameInLose();
                break;
            default:
                setGameInPlaying();
                break;
        }
    }

    private void setGameInMainMenu()
    { 
        Time.timeScale = 0;    
    }

    private void setGameInPlaying()
    {
        
        Time.timeScale = 1;
    }

    private void setGameInWin()
    {
        Time.timeScale = 0;
        winPanel.SetActive(true);
    }

    private void setGameInLose(){
        Time.timeScale = 0;
    }

    private void setGameInPause()
    {
        Time.timeScale = 0;
    }

    public GameStates getGameState()
    {
        return this.gameState;
    }

    public void setGameState(GameStates gm)
    {
        this.gameState = gm;
        gameStateUpdate();
    }

    public GameStates GetGameStates()
    {
        return this.gameState;
    }

    public void SaveGame()
    {
        SaveLoadManager.SaveGame(hp, rupies, medialHerbs, ammo, bomb, keys, atackLv, defenseLv);
    }

    public void LoadGame()
    {
        GameData data = SaveLoadManager.LoadGame();

        SceneManager.LoadScene("Scene1");

    }
}
